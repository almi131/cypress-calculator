// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add('add', (num1, num2) => {
     cy.get(num1).click();
     cy.get('#BtnPlus').click();
     cy.get(num2).click();
     cy.get('#BtnCalc').click()
    })

    Cypress.Commands.add('minus', (num1, num2, num3) => {
        cy.get(num1).click();
        cy.get(num2).click();
        cy.get('#BtnMinus').click();
        cy.get(num3).click();
        cy.get('#BtnCalc').click()
       })

       Cypress.Commands.add('mix', (num1, num2, num3, num4) => {
        cy.get('#BtnParanL').click()
        cy.get(num1).click();
        cy.get(num2).click();
        cy.get('#BtnMinus').click();
        cy.get(num3).click();
        cy.get('#BtnParanR').click()
        cy.get('#BtnMult').click()
        cy.get(num4).click();
        cy.get('#BtnCalc').click()
       })

       Cypress.Commands.add('sin', (num1, num2) => {
        cy.get('#BtnSin').click();
        cy.get('#BtnParanL').click();
        cy.get(num1).click();
        cy.get(num2).click();
        cy.get('#BtnParanR').click();
        cy.get('#BtnCalc').click()
       })