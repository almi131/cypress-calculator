import { numbers } from "../../PageObjects";
describe("Verify that the user is able to add", () => {
    beforeEach(() => {
      cy.visit("https://web2.0calc.com/");
      cy.wait(200);
    });

    it("Should calculate multiple", () => {
        //sum
        cy.add('#Btn4','#Btn1')
        cy.get('#input').should('have.value', '5')
        cy.get('#BtnClear').click()

        //subtraction
        cy.minus('#Btn1','#Btn2','#Btn6')
        cy.get('#input').should('have.value', '6')
        cy.get('#BtnClear').click()

        //mix
        cy.mix('#Btn1','#Btn4','#Btn2','#Btn2')
        cy.get('#input').should('not.have.value', '20')
        cy.get('#BtnClear').click()

        //sin
        cy.sin('#Btn3','#Btn0')
        cy.get('#input').should('have.value', '0.5')
        cy.get('#BtnClear').click()
   
        //Verify Hystory
        //possible to create functional for history 
        //not working for me but all elemets are correct , should fix
       cy.get(':nth-child(4) > .l').click
       cy.get('#input').should('have.value', '4+1')
       cy.get('#BtnClear').click()
       cy.get(':nth-child(4) > .r').click
       cy.get('#input').should('have.value', '5')
       cy.get('#BtnClear').click()

       cy.get(':nth-child(3) > .l').click
       cy.get('#input').should('have.value', '12 - 6')
       cy.get('#BtnClear').click()
       cy.get(':nth-child(3) > .r').click
       cy.get('#input').should('have.value', '6')
       cy.get('#BtnClear').click()

       cy.get(':nth-child(2) > .l').click
       cy.get('#input').should('have.value', '(14 - 2) * 2')
       cy.get('#BtnClear').click()
       cy.get(':nth-child(3) > .r').click
       cy.get('#input').should('have.value', '24')
       cy.get('#BtnClear').click()

       cy.get(':nth-child(1) > .l').click
       cy.get('#input').should('have.value', 'sin(30)')
       cy.get('#BtnClear').click()
       cy.get(':nth-child(1) > .r').click
       cy.get('#input').should('have.value', '0.5')
       cy.get('#BtnClear').click()
})
})