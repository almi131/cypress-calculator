import { numbers } from "../../PageObjects";
describe("Verify that the user is able to add", () => {
    beforeEach(() => {
      cy.visit("https://web2.0calc.com/");
      cy.wait(200);
    });
    it("Should calculate 4 + 1 = 5", () => {
     cy.add('#Btn4','#Btn1')
     cy.get('#input').should('have.value', '5')
  })
})
 